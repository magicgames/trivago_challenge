<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170602094029 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE hotel_stats (id INTEGER NOT NULL, totalReviews INTEGER NOT NULL, positiveReviews INTEGER NOT NULL, negativeReviews INTEGER NOT NULL, positiveScore NUMERIC(10, 2) NOT NULL, negativeScore NUMERIC(10, 2) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE review (id INTEGER NOT NULL, hotel_id INTEGER DEFAULT NULL, review CLOB NOT NULL, score INTEGER NOT NULL, is_processed BOOLEAN DEFAULT \'0\' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_794381C63243BB18 ON review (hotel_id)');
        $this->addSql('CREATE INDEX processed_idx ON review (is_processed)');
        $this->addSql('CREATE TABLE review_topic_result (id INTEGER NOT NULL, topic_id INTEGER DEFAULT NULL, review_id INTEGER DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E9A009301F55203D ON review_topic_result (topic_id)');
        $this->addSql('CREATE INDEX IDX_E9A009303E2E969B ON review_topic_result (review_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E9A009301F55203D3E2E969B ON review_topic_result (topic_id, review_id)');
        $this->addSql('CREATE TABLE review_word_result (id INTEGER NOT NULL, review_id INTEGER DEFAULT NULL, word_id INTEGER DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1F75F9E13E2E969B ON review_word_result (review_id)');
        $this->addSql('CREATE INDEX IDX_1F75F9E1E357438D ON review_word_result (word_id)');
        $this->addSql('CREATE TABLE topic (id INTEGER NOT NULL, parent_topic_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX parent_idx ON topic (parent_topic_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9D40DE1B5E237E0680988CEC ON topic (name, parent_topic_id)');
        $this->addSql('CREATE TABLE word (id INTEGER NOT NULL, name VARCHAR(255) NOT NULL, score INTEGER NOT NULL, is_auto_populated BOOLEAN DEFAULT \'0\' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX score_idx ON word (score)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C3F175115E237E06 ON word (name)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE hotel_stats');
        $this->addSql('DROP TABLE review');
        $this->addSql('DROP TABLE review_topic_result');
        $this->addSql('DROP TABLE review_word_result');
        $this->addSql('DROP TABLE topic');
        $this->addSql('DROP TABLE word');
    }
}
