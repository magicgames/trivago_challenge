<?php
/**
 * Created by PhpStorm.
 * User: ballbrk
 * Date: 30/05/2017
 * Time: 15:27
 */

namespace SemanticBundle\Interfaces;

use Symfony\Component\Finder\SplFileInfo;

interface CsvImportInterface
{
    /**
     * import csv file to the entity
     * @param SplFileInfo $file
     */
    public function importFromCsv($file);
}
