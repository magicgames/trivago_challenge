<?php
/**
 * Created by PhpStorm.
 * User: ballbrk
 * Date: 30/05/2017
 * Time: 15:27
 */

namespace SemanticBundle\Interfaces;

interface SemanticProcessInterface
{
    /**
     * @param \SemanticBundle\Entity\Word[] $positiveWords
     * @param \SemanticBundle\Entity\Word[] $negativeWords
     * @param \SemanticBundle\Entity\Word[] $removeWords
     * @param \SemanticBundle\Entity\Topic[] $topics
     * @param \SemanticBundle\Entity\Review[] $reviews
     *
     *  @return array see example
     *  @example The returned array consists of this
     *
     * {
     * "review_id" => XXX ,
     * "topics" => yyy ,
     * "attributes =>
     *  {
     *      "positive"=>array(name=>score),
     *      "negative"=>array(name=>score)
     *   }
     * }
     */
    public function extract(array $positiveWords, array $negativeWords, array $removeWords, array $topics, array $reviews);
}
