<?php
/**
 * Created by PhpStorm.
 * User: ballbrk
 * Date: 30/05/2017
 * Time: 15:27
 */

namespace SemanticBundle\Interfaces;

interface CustomRepositoryInterface
{
    /**
     * @param $object
     * @param bool $flush
     */
    public function save($object, $flush = true);

    /**
     * @param $object
     * @param bool $flush
     */
    public function remove($object, $flush = true);


    public function searchWord($searchField, $searchString, $searchOper, $field, $ord);
    public function findAllSort($field, $sord);

    /**
     * @param array $criteria
     *  public function findOneOrCreate(array $criteria);
     */

}
