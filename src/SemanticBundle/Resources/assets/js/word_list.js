( function ($) {
        'use strict';
        var word_list = {
            init: function () {
                var grid = $('#word-grid');
                var addOptions;
                var editOptions;
                var deleteOptions;
                var searchOptions;

                searchOptions = {

                    searchCaption: "Search Topic",
                    searchtext: "Search Topic",
                    closeOnEscape: true,
                    closeAfterSearch: true,
                    width: "500",
                    reloadAfterSubmit: true
                };
                deleteOptions = {
                    deleteCaption: "delete Post",
                    deletetext: "Delete Post",
                    closeOnEscape: true,
                    closeAfterEdit: true,
                    width: "500",
                    reloadAfterSubmit: true,
                    bottominfo: "Fields marked with (*) are required",
                    top: "60",
                    left: "5",
                    right: "5",
                    onclickSubmit: function (response, postdata) {

                    }
                };
                addOptions = {
                    /* Add */
                    addCaption: "Add Topic",
                    addtext: "Add",
                    closeOnEscape: true,
                    closeAfterAdd: true,
                    width: 500,
                    bottominfo: "Fields marked with (*) are required",

                };
                editOptions = {
                    /* Edit */
                    reloadAfterSubmit: true,
                    closeOnEscape: true,
                    closeAfterEdit: true,
                    width: 500,
                    bottominfo: "Fields marked with (*) are required"
                };

                grid.jqGrid({
                    datatype: 'json',
                    url: app.wordList,
                    editurl: app.wordEdit,
                    colNames: ['id', 'name','score','auto popuplate'],
                    colModel: [
                        {name: 'id', index: 'id', width: 10},
                        {
                            name: 'name',
                            index: 'name',
                            formoptions: {
                                label: 'Name*'
                            },
                            editrules: {
                                edithidden: true,
                                required: true
                            },
                            editable: true
                        },
                        {
                            name: 'score',
                            index: 'score',
                            formoptions: {
                                label: 'score*'
                            },
                            editrules: {
                                edithidden: true,
                                required: true,
                                number:true
                            },
                            editable: true

                        },
                        {
                            name: 'is_auto_populated',
                            index: 'isAutoPopulated',
                            formoptions: {
                                label: 'score*'
                            },
                            editrules: {
                                edithidden: true,
                                required: true
                            },
                            editable: false
                        }
                    ],
                    caption: "Words",
                    gridview: true,
                    autowidth: true,
                    shrinkToFit: true,
                    viewrecords: true,
                    toppager: true,
                    rowList: [1, 5, 10, 20, "10000:All"],
                    rowNum: 10,
                    guiStyle: "bootstrap",
                    iconSet: "fontAwesome",
                    multiselect: false,
                    sortname: 'id',
                    pager: '#pager',
                    sortorder: "desc",
                    afterSubmit: function (response, postdata) {
                        var data = JSON.parse(response.responseText);
                        if(data.status ==='OK'){
                            //for successfull editing return true.
                            return [true,"Success"]; //  return [success,message,new_id]
                        }else{
                            return [false,data.message];
                        }
                    },
                    rowattr: function (rd) {
                        var apply_css='';
                        if (parseInt(rd.score) < 0) {
                            apply_css = 'bg-danger';
                        }  else if (parseInt(rd.score) > 0) {
                            apply_css = 'bg-success';
                        } else {
                            apply_css='';
                        }
                        return {"class": apply_css};
                    },
                    onSelectRow: function (ids) {

                    }


                });
                grid.jqGrid("navGrid", {
                        edit: true,
                        add: true,
                        del: true,
                        search: true,
                        refresh: true,
                        view: true
                    },
                    editOptions,
                    addOptions,
                    deleteOptions,
                    searchOptions,
                    {closeOnEscape: true})
                    .jqGrid("inlineNav")
                    .jqGrid("filterToolbar")
                    .jqGrid("gridResize");
            }
        };


        $(document).ready(function () {
            word_list.init();
        })
    }(jQuery)
);
