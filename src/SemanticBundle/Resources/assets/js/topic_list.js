( function ($) {
        'use strict';
        var topic_list = {
            init: function () {
                var grid = $('#topic-grid');
                var subgrid = $('#topic-subgrid');
                var addOptions;
                var editOptions;
                var deleteOptions;
                var searchOptions;

                searchOptions = {

                    searchCaption: "Search Topic",
                    searchtext: "Search Topic",
                    closeOnEscape: true,
                    closeAfterSearch: true,
                    width: "500",
                    reloadAfterSubmit: true
                };
                deleteOptions = {
                    deleteCaption: "delete Topic",
                    deletetext: "Delete Topic",
                    closeOnEscape: true,
                    closeAfterEdit: true,
                    width: "500",
                    reloadAfterSubmit: true,
                    bottominfo: "Fields marked with (*) are required",
                    top: "60",
                    left: "5",
                    right: "5",
                    onclickSubmit: function (response, postdata) {

                    }
            };

                addOptions =   {/* Add */
                    addCaption: "Add Topic",
                    addtext: "Add",
                    closeOnEscape: true,
                    closeAfterAdd: true,
                    width: 500,
                    bottominfo: "Fields marked with (*) are required"

                };
                editOptions = {
                    /* Edit */
                    reloadAfterSubmit: true,
                    closeOnEscape: true,
                    closeAfterEdit: true,
                    width: 500,
                    bottominfo: "Fields marked with (*) are required"

                };
                grid.jqGrid({
                    datatype: 'json',
                    url: app.topicList,
                    editurl: app.topicList + '/edit',
                    caption: "Parent Topics",
                    colNames: ['id', 'name'],
                    colModel: [
                        {name: 'id', index: 'id', width: 10},
                        {
                            name: 'name',
                            index: 'name',
                            formoptions: {
                                label: 'Name*'
                            },
                            editrules: {
                                edithidden: true,
                                required: true
                            },
                            editable: true
                        }
                    ],
                    gridview: true,
                    autowidth: true,
                    shrinkToFit: true,
                    viewrecords: true,
                    toppager: true,
                    rowList: [1, 5, 10, 20, "10000:All"],
                    rowNum: 10,
                    guiStyle: "bootstrap",
                    iconSet: "fontAwesome",
                    multiselect: false,
                    sortname: 'id',
                    pager: '#pager',
                    sortorder: "desc",
                    recreateFilter:true,
                    afterSubmit: function (response, postdata) {
                        var data = JSON.parse(response.responseText);
                        if(data.status ==='OK'){
                            //for successfull editing return true.
                            return [true,"Success"]; //  return [success,message,new_id]
                        }else{
                            return [false,data.message];
                        }
                    },
                    onSelectRow: function (ids) {
                        if (ids === null) {
                            ids = 0;
                            if (subgrid.jqGrid('getGridParam', 'records') > 0) {
                                subgrid.jqGrid('setGridParam', {
                                    url: app.topicList + "/" + ids,
                                    editurl: app.topicList + "/" + ids+ '/edit',
                                    page: 1});
                                subgrid.jqGrid('setCaption', "Alternate Topics: " + ids)
                                    .trigger('reloadGrid');
                            }
                        } else {
                            subgrid.jqGrid('setGridParam', {
                                url: app.topicList + "/" + ids,
                                editurl: app.topicList + "/" + ids+ '/edit',
                                page: 1});
                            subgrid.jqGrid('setCaption', "Alternate Topics: " + ids)
                                .trigger('reloadGrid');
                        }
                    }


                });
                grid.jqGrid("navGrid", {
                        edit: true,
                        add: true,
                        del: true,
                        search: true,
                        refresh: true,
                        view: true
                    },
                    editOptions,
                    addOptions,
                    deleteOptions,
                    searchOptions,
                    {closeOnEscape: true})
                    .jqGrid("inlineNav")
                    .jqGrid("filterToolbar")
                    .jqGrid("gridResize");


                subgrid.jqGrid({
                    datatype: 'json',
                    url: app.topicList + "/" + 0,
                    caption: "Alternate Topics",
                    colNames: ['id', 'name'],
                    colModel: [
                        {name: 'id', width: 10},
                        {
                            name: 'name',
                            index: 'name',
                            formoptions: {
                                label: 'Name*'
                            },
                            editrules: {
                                edithidden: true,
                                required: true}
                            ,
                            editable: true
                        }
                    ],
                    afterSubmit: function (response, postdata) {
                        var data = JSON.parse(response.responseText);
                        if(data.status ==='OK'){
                            //for successfull editing return true.
                            return [true,"Success"]; //  return [success,message,new_id]
                        }else{
                            return [false,data.message];
                        }
                    },
                    gridview: true,
                    autowidth: true,
                    shrinkToFit: true,
                    viewrecords: true,
                    toppager: true,
                    rowList: [1, 5, 10, 20, "10000:All"],
                    rowNum: 10,
                    guiStyle: "bootstrap",
                    iconSet: "fontAwesome",
                    multiselect: false,
                    sortname: 'id',
                    pager: '#pager_subgrid',
                    sortorder: "desc",
                    recreateFilter:true


                });

                subgrid.jqGrid("navGrid", {
                        edit: true,
                        add: true,
                        del: true,
                        search: true,
                        refresh: true,
                        view: true
                    },
                    editOptions,
                    addOptions,
                    deleteOptions,
                    searchOptions,
                    {closeOnEscape: true})
                    .jqGrid("inlineNav")
                    .jqGrid("filterToolbar")
                    .jqGrid("gridResize");


            }
        };


        $(document).ready(function () {
            topic_list.init();
        })
    }(jQuery)
);
