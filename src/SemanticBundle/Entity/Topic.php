<?php

namespace SemanticBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use JMS\Serializer\Annotation as JMS;

/**
 * Topic
 *
 * @JMS\ExclusionPolicy("none")
 * @ORM\Table(name="topic",
 *     indexes={@ORM\Index(name="parent_idx", columns={"parent_topic_id"})},
 *     uniqueConstraints={@ORM\UniqueConstraint(columns={"name", "parent_topic_id"})})
 * @ORM\Entity(repositoryClass="SemanticBundle\Repository\TopicRepository")
 * @UniqueEntity(fields={"name", "parentTopic"})
 */
class Topic
{

    use TimestampableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * @ORM\ManyToOne(targetEntity="Topic", inversedBy="childTopic")
     * @ORM\JoinColumn(name="parent_topic_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parentTopic;

    /**
     * @ORM\OneToMany(targetEntity="Topic", mappedBy="parentTopic",cascade={"persist", "remove"})
     * @JMS\Exclude
     */
    private $childTopic;


    /**
     * @ORM\OneToMany(targetEntity="ReviewTopicResult", mappedBy="topic",cascade={"persist", "remove"})
     * @JMS\Exclude
     */
    private $topicResult;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->childTopic = new ArrayCollection();
        $this->topicResult = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Topic
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set parentTopic
     *
     * @param \SemanticBundle\Entity\Topic $parentTopic
     *
     * @return Topic
     */
    public function setParentTopic(Topic $parentTopic = null)
    {
        $this->parentTopic = $parentTopic;

        return $this;
    }

    /**
     * Get parentTopic
     *
     * @return \SemanticBundle\Entity\Topic
     */
    public function getParentTopic()
    {
        return $this->parentTopic;
    }

    /**
     * Add childTopic
     *
     * @param \SemanticBundle\Entity\Topic $childTopic
     *
     * @return Topic
     */
    public function addChildTopic(Topic $childTopic)
    {
        $this->childTopic[] = $childTopic;

        return $this;
    }

    /**
     * Remove childTopic
     *
     * @param \SemanticBundle\Entity\Topic $childTopic
     */
    public function removeChildTopic(Topic $childTopic)
    {
        $this->childTopic->removeElement($childTopic);
    }

    /**
     * Get childTopic
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildTopic()
    {
        return $this->childTopic;
    }

    /**
     * Add reviewTopicResult
     *
     * @param \SemanticBundle\Entity\ReviewTopicResult $topicResult
     *
     * @return Topic
     */
    public function addReviewTopicResult(ReviewTopicResult $topicResult)
    {
        $this->topicResult[] = $topicResult;

        return $this;
    }

    /**
     * Remove reviewTopicResult
     *
     * @param \SemanticBundle\Entity\ReviewTopicResult $topicResult
     */
    public function removeReviewTopicResult(ReviewTopicResult $topicResult)
    {
        $this->topicResult->removeElement($topicResult);
    }

    /**
     * Get reviewTopicResult
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviewTopicResult()
    {
        return $this->topicResult;
    }
}
