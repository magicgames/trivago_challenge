<?php

namespace SemanticBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Timestampable\Traits\TimestampableEntity;
/**
 * ReviewWordResult
 * @JMS\ExclusionPolicy("none")
 * @ORM\Table(name="review_word_result")
 * @ORM\Entity(repositoryClass="SemanticBundle\Repository\ReviewWordResultRepository")
 */
class ReviewWordResult
{

    use TimestampableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Review", inversedBy="reviewWordResult",)
     * @ORM\JoinColumn(name="review_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $review;



    /**
     * @ORM\ManyToOne(targetEntity="Word", inversedBy="wordResult")
     * @ORM\JoinColumn(name="word_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $word;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set review
     *
     * @param \SemanticBundle\Entity\Review $review
     *
     * @return ReviewWordResult
     */
    public function setReview(\SemanticBundle\Entity\Review $review = null)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return \SemanticBundle\Entity\Review
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set word
     *
     * @param \SemanticBundle\Entity\Word $word
     *
     * @return ReviewWordResult
     */
    public function setWord(\SemanticBundle\Entity\Word $word = null)
    {
        $this->word = $word;

        return $this;
    }

    /**
     * Get word
     *
     * @return \SemanticBundle\Entity\Word
     */
    public function getWord()
    {
        return $this->word;
    }
}
