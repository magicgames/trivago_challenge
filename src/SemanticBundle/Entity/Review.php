<?php

namespace SemanticBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
/**
 * Review
 * @JMS\ExclusionPolicy("none")
 * @ORM\Table(name="review",indexes={@ORM\Index(name="processed_idx", columns={"is_processed"})})
 * @ORM\Entity(repositoryClass="SemanticBundle\Repository\ReviewRepository")
 */
class Review
{


    use TimestampableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="hotel", type="integer")
     */

    /**
     * Many Reviews have One HotelStats.
     * @ORM\ManyToOne(targetEntity="HotelStats",fetch="EAGER")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id")
     *
     */

    private $hotel;

    /**
     * @var string
     *
     * @ORM\Column(name="review", type="text")
     */
    private $review;

    /**
     * @var int
     *
     * @ORM\Column(name="score", type="integer",options={"defaults":0})
     */
    private $score = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_processed", type="boolean",options={"default":false, "comment":"Check if the review is processed"})
     */
    private $isProcessed = false;


    /**
     * @ORM\OneToMany(targetEntity="ReviewWordResult", mappedBy="review",cascade={"remove"})
     */
    private $reviewWordResult;

    /**
     * @ORM\OneToMany(targetEntity="ReviewTopicResult", mappedBy="review",cascade={"remove"})
     */
    private $reviewTopicResult;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reviewWordResult = new ArrayCollection();
        $this->reviewTopicResult = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set review
     *
     * @param string $review
     *
     * @return Review
     */
    public function setReview($review)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return string
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set isProcessed
     *
     * @param boolean $isProcessed
     *
     * @return Review
     */
    public function setIsProcessed($isProcessed)
    {
        $this->isProcessed = $isProcessed;

        return $this;
    }

    /**
     * Get isProcessed
     *
     * @return boolean
     */
    public function getIsProcessed()
    {
        return $this->isProcessed;
    }



    /**
     * Set score
     *
     * @param integer $score
     *
     * @return Review
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }



    /**
     * Set hotel
     *
     * @param \SemanticBundle\Entity\HotelStats $hotel
     *
     * @return Review
     */
    public function setHotel(\SemanticBundle\Entity\HotelStats $hotel = null)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return HotelStats
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Add reviewWordResult
     *
     * @param ReviewWordResult $reviewWordResult
     *
     * @return Review
     */
    public function addReviewWordResult(ReviewWordResult $reviewWordResult)
    {
        $this->reviewWordResult[] = $reviewWordResult;

        return $this;
    }

    /**
     * Remove reviewWordResult
     *
     * @param ReviewWordResult $reviewWordResult
     */
    public function removeReviewWordResult(ReviewWordResult $reviewWordResult)
    {
        $this->reviewWordResult->removeElement($reviewWordResult);
    }

    /**
     * Get reviewWordResult
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviewWordResult()
    {
        return $this->reviewWordResult;
    }

    /**
     * Add reviewTopicResult
     *
     * @param \SemanticBundle\Entity\ReviewTopicResult $reviewTopicResult
     *
     * @return Review
     */
    public function addReviewTopicResult(ReviewTopicResult $reviewTopicResult)
    {
        $this->reviewTopicResult[] = $reviewTopicResult;

        return $this;
    }

    /**
     * Remove reviewTopicResult
     *
     * @param ReviewTopicResult $reviewTopicResult
     */
    public function removeReviewTopicResult(ReviewTopicResult $reviewTopicResult)
    {
        $this->reviewTopicResult->removeElement($reviewTopicResult);
    }

    /**
     * Get reviewTopicResult
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviewTopicResult()
    {
        return $this->reviewTopicResult;
    }
}
