<?php

namespace SemanticBundle\Repository;

use  \Doctrine\Common\Collections\Criteria;

use Doctrine\ORM\EntityRepository;
use  SemanticBundle\Interfaces\CustomRepositoryInterface;

abstract class BaseRepositoryAbstract extends EntityRepository implements CustomRepositoryInterface
{
    public function save($object, $flush = true)
    {
        $this->getEntityManager()->persist($object);
        if ($flush) {
            $this->getEntityManager()->flush();
        }

        return;
    }

    public function remove($object, $flush = true)
    {
        $this->getEntityManager()->remove($object);
        if ($flush) {
            $this->getEntityManager()->flush();
        }

        return;
    }

    public function searchWord($search_field, $search_string, $search_oper, $field, $ord)
    {
        $query = $this->createQueryBuilder('w');
        $results = $query->select('w');
        switch ($search_oper) {

            case 'eq':#<option value="eq" selected="selected">equal</option>
                $results->where('w.'.$search_field.'=:name');
                $results->setParameter('name', $search_string);
                break;

            case 'ne':#<option value="ne">not equal</option>
                $results->where('w.'.$search_field.'!= :name');
                $results->setParameter('name', $search_string);
                break;

            case 'bw':#<option value="bw">begins with</option>
                $literal = $query->expr()->literal($search_string."%");
                $results->where($query->expr()->like('w.'.$search_field, $literal));
                break;

            case 'bn':#<option value="bn">does not begin with</option>
                $literal = $query->expr()->literal($search_string."%");
                $results->where($query->expr()->notLike('w.'.$search_field, $literal));
                break;

            case 'ew':#<option value="ew">ends with</option>
                $literal = $query->expr()->literal("%".$search_string);
                $results->where($query->expr()->like('w.'.$search_field, $literal));
                break;
            case 'en':#<option value="en">does not end with</option>
                $literal = $query->expr()->literal("%".$search_string);
                $results->where($query->expr()->notLike('w.'.$search_field, $literal));
                break;
            case 'cn':#<option value="cn">contains</option>
                $literal = $query->expr()->literal("%".$search_string."%");
                $results->where($query->expr()->like('w.'.$search_field, $literal));

                break;

            case 'nc':#<option value="nc">does not contain</option>
                $literal = $query->expr()->literal("%".$search_string."%");
                $results->where($query->expr()->notLike('w.'.$search_field, $literal));
                break;

            case 'nu':#<option value="nu">is null</option>
                $results->where($query->expr()->isNull('w.'.$search_field));
                break;

            case 'nn':#<option value="nn">is not null</option>
                $results->where($query->expr()->isNotNull('w.'.$search_field));
                break;

            case 'in':#<option value="in">is in</option>
                $results->where($query->expr()->in('w.'.$search_field, $search_string));
                break;

            case 'ni':#<option value="ni">is not in</option>
                $results->where($query->expr()->notIn('w.'.$search_field, $search_string));
                break;
        }
        $ord = (strtolower($ord) == 'desc') ? 'desc' : 'asc';
        $results->orderBy('w.'.$field, $ord);

        return $results;
    }

    public function findAllSort($field, $sord)
    {
        $sord = (strtolower($sord) == 'desc') ? 'desc' : 'asc';

        return $this->findBy(array(), array($field => $sord));
    }


}
