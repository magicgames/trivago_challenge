<?php

namespace SemanticBundle\Repository;

use Symfony\Component\Security\Acl\Exception\Exception;

/**
 * HotelStatsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class HotelStatsRepository extends BaseRepositoryAbstract
{
    public function prepareStats()
    {
        $query = $this->createQueryBuilder('t');
        $results = $query->update('SemanticBundle:HotelStats', 't')
            ->set('t.positiveReviews', '0')
            ->set('t.negativeReviews', '0')
            ->set('t.negativeScore', '0')
            ->set('t.positiveScore', '0')
            ->getQuery()
            ->execute();
        return $results;
    }

    public function findOneOrCreate(array $criteria)
    {
        if (array_key_exists('id', $criteria)) {
            $entity = $this->find($criteria['id']);
        } else {
           throw new \Exception("Hotel id can't be null ");
        }
        if ($entity === null) {
            $result['created']=true;
            $entity_str = parent::getEntityName();
            $entity = new $entity_str;
            $entity->setId($criteria['id']);
            $this->save($entity, true);
        } else {
            $result['created']=false;
        }
        $result['entity']=$entity;
        return $result;
    }

    public function updateIfNew($hotel,$new=true) {

        if ($new) {
            $this->save($hotel,true);
        }
        return;

    }


}
