<?php

namespace AnalyzerBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Finder\Finder;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadTopicData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $finder = new Finder();
        $finder->files()->in('src/SemanticBundle/Resources/datasets')->name('topic.csv');
        foreach ($finder as $file) {
            $import = $this->container->get('semantic.service.import_topic');
            $import->importFromCsv($file);
            $import = $this->container->get('semantic.service.import_word');
            $import->importFromCsv($file);
        }
    }
}
