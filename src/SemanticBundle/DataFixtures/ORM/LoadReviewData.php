<?php
/**
 *
 */

namespace AnalyzerBundle\DataFixtures\ORM;

use SemanticBundle\Entity\Review;
use SemanticBundle\Entity\HotelStats;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SemanticBundle\Repository\HotelStatsRepository;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use SplFileObject;
use Symfony\Component\Finder\Finder;

class LoadReviewData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $finder = new Finder();
        $finder->files()->in('src/SemanticBundle/Resources/datasets')->name('reviews.csv');
        foreach ($finder as $file) {
            $import = $this->container->get('semantic.service.import_review');
            $import->importFromCsv($file);
        }
    }
}
