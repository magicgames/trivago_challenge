<?php

namespace SemanticBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;

/**
 * Review controller.
 *
 * @Route("review")
 */
class ReviewController extends Controller
{
    /**
     * Lists all review entities.
     *
     * @Route("/", name="review_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $limit = $request->get('rows', 10);
        $page = $request->get('page', 1);
        $words = $this->get('semantic.service.review_crud')->listReview();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $words,
            $page,
            $limit,
            array()
        );
        $serializer = $this->get('jms_serializer');
        $response = $serializer->serialize($pagination, 'json');

        return new Response($response);
    }

    /**
     * Lists all review entities.
     *
     * @Route("/edit", name="review_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request)
    {
        $result = $this->get('semantic.service.review_crud')->processReview();
        $serializer = $this->container->get('jms_serializer');
        $jsonContent = $serializer
            ->serialize(
                $result,
                'json',
                SerializationContext::create()
                    ->enableMaxDepthChecks()
            );

        return new Response($jsonContent);
    }

    /**
     * @param Request $request
     * @Route("/upload", name="review_upload")
     * @Method({"GET", "POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function uploadReviewsAction(Request $request)
    {
        $reviewService = $this->get('semantic.service.import_review');
        $uploaderForm = $this->createForm('SemanticBundle\Form\UploadReviewType');
        $uploaderForm->handleRequest($request);
        if ($uploaderForm->isValid() && $uploaderForm->isSubmitted()) {

            try {
                $raw_file = $uploaderForm['file']->getData();
                if ($raw_file == null) {
                    throw new \Exception('File does not exist');
                }
                $reviewService->importFromCsv($raw_file);
                $this->addFlash('success','File Uploaded correctly');
            } catch (\Exception $e)
            {
                $this->addFlash('danger',$e->getMessage());
            }


        } else {
            $this->addFlash('danger', 'Error in form');

        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/run", name="review_run")
     * @Method({"GET", "POST"})
     * @return Response
     */
    public function runReviewsAction()
    {
        $result = $this->get('semantic.service.process_review')->checkReview();
        $serializer = $this->container->get('jms_serializer');
        $jsonContent = $serializer
            ->serialize(
                $result,
                'json',
                SerializationContext::create()
                    ->enableMaxDepthChecks()
            );

        return new Response($jsonContent);
    }



    /**
     * @Route("/full_run", name="review_full_run")
     * @Method({"GET", "POST"})
     * @return Response
     */
    public function runFullReviewsAction()
    {
        $em = $this->getDoctrine()->getRepository('SemanticBundle:Review');
        $em->prepareReviews();
        $em = $this->getDoctrine()->getRepository('SemanticBundle:HotelStats');
        $em->prepareStats();
        $em = $this->getDoctrine()->getRepository('SemanticBundle:ReviewTopicResult');
        $em->deleteAll();
        $em = $this->getDoctrine()->getRepository('SemanticBundle:ReviewWordResult');
        $em->deleteAll();

        return $this->runReviewsAction();
    }
}
