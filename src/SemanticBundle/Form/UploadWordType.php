<?php

namespace SemanticBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UploadWordType extends AbstractType
{


    /**
     * @var Router
     */
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'file',
            FileType::class,
            array(
                'label' => 'File',
                'mapped' => false,
            )
        )
            ->add(
                'save',
                SubmitType::class,
                array('label' => 'Upload')
            );
        $builder->setAction(
            $this->router->generate('upload_word')
        );
    }
}
