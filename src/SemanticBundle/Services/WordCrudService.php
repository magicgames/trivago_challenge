<?php
/**
 * Created by PhpStorm.
 * User: ballbrk
 * Date: 28/05/2017
 * Time: 09:50
 */

namespace SemanticBundle\Services;

use SemanticBundle\Utils\TextSanitize;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityRepository;

class WordCrudService
{

    /**
     * @var EntityRepository
     */
    private $wordRepository;

    protected $requestStack;

    protected $textSanitize;

    public function __construct(
        RequestStack $requestStack,
        EntityRepository $wordRepository,
        TextSanitize $textSanitize
    ) {
        $this->requestStack = $requestStack;
        $this->wordRepository = $wordRepository;
        $this->textSanitize = $textSanitize;
    }


    public function listWord()
    {
        $request = $this->requestStack->getCurrentRequest();
        $request = $request->query;

        $search = $request->get('_search', "false");
        $ord = $request->get('sord', 'asc');
        $field = $request->get('sidx', 'id');
        $field = ($field !== '') ? $field : 'id';
        if ($search == "true") {
            $search_field = $request->get('searchField', false);
            $search_string = $request->get('searchString', false);
            $search_oper = $request->get('searchOper', false);
            $words = $this->wordRepository->searchWord(
                $search_field,
                $search_string,
                $search_oper,
                $field,
                $ord
            );
        } else {
            $words = $this->wordRepository->findAllSort($field, $ord);
        }
        return $words;
    }
    public function processWord()
    {
        $request = $this->requestStack->getCurrentRequest();

        $request = $request->request;

        $oper = $request->get("oper");
        $name = trim(mb_strtolower($this->textSanitize->sanitize($request->get("name"))));
        $score = $request->get("score");
        $id = $request->get('id');
        if (is_numeric($id)) {
            $criteria = array('name' => $name,'id'=>$id,'score'=>$score);
        } else {
            $criteria = array('name' => $name, 'score' => $score);
        }
        if(!is_numeric($score)) {
            unset($criteria['score']);
        }
        try {
            switch ($oper) {
                case 'add':
                    $word = $this->wordRepository->findOneOrCreate($criteria);
                    $result['result']=$word;
                    break;
                case 'del':
                    $word = $this->wordRepository->find($id);
                    $this->wordRepository->remove($word, true);
                    break;
                case 'edit':
                    $word = $this->wordRepository->update($criteria);
                    $result['result']=$word;
                    break;
            }
            $result['status'] = 'OK';
        } catch (\Exception $e) {
            $result['status'] = 'KO';
            $result['message'] = $e->getMessage();
            $result['code'] = $e->getCode();
        }

        return $result;
    }
}
