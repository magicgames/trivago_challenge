<?php
/**
 * Created by PhpStorm.
 * User: ballbrk
 * Date: 30/05/2017
 * Time: 14:48
 */

namespace SemanticBundle\Services;

use SemanticBundle\Entity\Word;
use Doctrine\ORM\EntityRepository;
use Ddeboer\DataImport\Reader\CsvReader;
use SplFileObject;
use Symfony\Component\HttpFoundation\File\File;
use SemanticBundle\Utils\TextSanitize;
use SemanticBundle\Interfaces\CsvImportInterface;
use Symfony\Component\Finder\SplFileInfo;

class ImportWordService implements CsvImportInterface
{
    /**
     * @var EntityRepository
     */
    private $wordRepository;

    /**
     * @var TextSanitize
     */
    private $textSanitize;

    /**
     * ImportTopicService constructor.
     * @param EntityRepository $wordRepository
     * @param TextSanitize $textSanitize
     */
    public function __construct(EntityRepository $wordRepository, TextSanitize $textSanitize)
    {
        $this->wordRepository = $wordRepository;
        $this->textSanitize = $textSanitize;
    }

    /**
     * @param File $file
     */
    public function importFromCsv($file)
    {
        $csv_file = new SplFileObject($file->getPathName());
        $reader = new CsvReader($csv_file);
        $reader->setHeaderRowNumber(0);
        $reader->setStrict(false);
        $topic = null;
        foreach ($reader as $row) {
            if (array_key_exists('Positives', $row) and $row['Positives'] != '') {
                $this->uploadWord($row['Positives'], 1);
            }
            if (array_key_exists('Negatives', $row) and $row['Negatives'] != '') {
                $this->uploadWord($row['Negatives'], -1);
            }
            if (array_key_exists('StopWords', $row) and $row['StopWords'] != '') {
                $this->uploadWord($row['StopWords'], 0);
            }
        }
        return;
    }

    private function uploadWord($name, $score)
    {
        $criteria = array('score'=>$score,'name' => mb_strtolower(trim($this->textSanitize->sanitize($name))));
        $this->wordRepository->findOneOrCreate($criteria);
        return;
    }
}
