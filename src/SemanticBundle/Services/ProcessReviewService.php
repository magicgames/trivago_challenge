<?php

namespace SemanticBundle\Services;

use SemanticBundle\Entity\Review;
use Doctrine\ORM\EntityRepository;
use SemanticBundle\Entity\ReviewWordResult;
use SemanticBundle\Entity\ReviewTopicResult;
use SemanticBundle\Entity\Word;
use SemanticBundle\Entity\Topic;

class ProcessReviewService
{
    /**
     * @var \SemanticBundle\Repository\WordRepository
     */
    private $wordRepository;

    /**
     * @var \SemanticBundle\Repository\TopicRepository
     */
    private $topicRepository;

    /**
     * @var \SemanticBundle\Repository\ReviewRepository
     */
    private $reviewRepository;

    /**
     * @var \SemanticBundle\Repository\ReviewTopicResultRepository
     */
    private $reviewTopicRepository;
    /**
     * @var \SemanticBundle\Repository\ReviewWordResultRepository
     */
    private $reviewWordRepository;
    /**
     * @var \SemanticBundle\Repository\HotelStatsRepository
     */
    private $hotelStatsRepository;

    /**
     * @var SemanticProcessService
     */
    private $semanticProcess;

    /**
     * ProcessReviewService constructor.
     * @param EntityRepository $wordRepository
     * @param EntityRepository $topicRepository
     * @param EntityRepository $reviewRepository
     * @param EntityRepository $reviewTopicRepository
     * @param EntityRepository $reviewWordRepository
     * @param EntityRepository $hotelStatsRepository
     * @param SemanticProcessService $semanticProcess
     */
    public function __construct(
        EntityRepository $wordRepository,
        EntityRepository $topicRepository,
        EntityRepository $reviewRepository,
        EntityRepository $reviewTopicRepository,
        EntityRepository $reviewWordRepository,
        EntityRepository $hotelStatsRepository,
        SemanticProcessService $semanticProcess
    ) {
        $this->wordRepository = $wordRepository;
        $this->topicRepository = $topicRepository;
        $this->reviewRepository = $reviewRepository;
        $this->reviewTopicRepository = $reviewTopicRepository;
        $this->reviewWordRepository = $reviewWordRepository;
        $this->hotelStatsRepository = $hotelStatsRepository;
        $this->semanticProcess = $semanticProcess;

        return;
    }

    /**
     *
     */
    public function checkReview()
    {
        $results = array();
        $response = array('status' => 'OK');
        $positiveWords = $this->wordRepository->getPositiveWords();
        $negativeWords = $this->wordRepository->getNegativeWords();
        $stopWords = $this->wordRepository->getNeutralWords();
        $topics = $this->prepareTopics();
        $reviews = $this->findNoProcessedReviews();
        if (count($reviews) == 0) {
            $message = "No reviews to process";

        } else {
            $results = $this->semanticProcess->extract(
                $positiveWords,
                $negativeWords,
                $stopWords,
                $topics,
                $reviews
            );

            if (count($results) === 0) {
                $message = "No Attributes in reviews";
            } else {
                foreach ($results as $result) {
                    $this->processReviewResult($result);
                }
                $message = "Review finished";

            }

        }


        $response['message'] = $message;

        return $response;
    }


    /**
     * @return array|Review[]
     */
    private function findNoProcessedReviews()
    {
        $criteria = array('isProcessed' => false);

        return $this->reviewRepository->findBy($criteria);
    }


    /**
     * @return array
     */
    private function prepareTopics()
    {
        return $this->topicRepository->prepareTopics();
    }


    /**
     * @param array $result
     * @return array
     */
    private function processReviewResult(array $result)
    {
        $response = array();

        $id = $result['review_id'];
        $positive = 0;
        $negative = 0;
        if (intval($id) > 0) {

            $review = $this->reviewRepository->find($id);
            $attributes = $result['attributes'];
            $topics = $result['topics'];

            if (count($attributes['positive']) > 0) {
                foreach ($attributes['positive'] as $key => $value) {
                    $positive = $positive + $value;
                    $this->processWord($key, $value, $review);
                }
            } else {
                $positive = 0;

            }
            if (count($attributes['negative']) > 0) {
                foreach ($attributes['negative'] as $key => $value) {
                    $negative = $negative + $value;
                    $this->processWord($key, $value, $review);
                }

            } else {
                $negative = 0;

            }

            if (count($topics) > 0) {
                foreach ($topics as $topic) {
                    $topicEntity = $this->topicRepository->findOneParent(trim($topic));
                    if ($topicEntity) {
                        $this->processTopic($topicEntity, $review);
                    }
                }

            }


            $total = $positive + $negative;
            $review->setScore($total);
            $review->setIsProcessed(true);
            $hotel = $review->getHotel();
            if ($total > 0) {
                $hotel->setPositiveReviews($hotel->getPositiveReviews() + 1);
            } else {
                $hotel->setNegativeReviews($hotel->getNegativeReviews() + 1);
            }
            $hotel->setPositiveScore($hotel->getPositiveScore() + $positive);
            $hotel->setNegativeScore($hotel->getNegativeScore() + $negative);
            $this->hotelStatsRepository->save($hotel, true);
            $this->reviewRepository->save($review, true);
        }

        $response['status'] = 'OK';


        return $response;
    }

    /**
     * @param $word_string
     * @param bool $isPositive
     * @param int $score
     * @param Review $review
     */
    private function processWord($wordString, $score = 0, Review $review)
    {
        $criteria = array('name' => $wordString);
        $word = $this->wordRepository->findOneBy($criteria);
        if (!$word) {
            $word = new Word();
            $word->setName($wordString);
            $word->setScore($score);
            $word->setIsAutoPopulated(true);
            $this->wordRepository->save($word, true);
        }
        $review_word = new ReviewWordResult();
        $review_word->setReview($review);
        $review_word->setWord($word);
        $this->reviewWordRepository->save($review_word, true);

        return;
    }

    /**
     * @param Topic $topic
     * @param $review
     */
    private function processTopic(Topic $topic, Review $review)
    {
        $criteria = array('topic' => $topic, 'review' => $review);
        $element = $this->reviewTopicRepository->findBy($criteria);
        if (!$element) {
            $review_topic = new ReviewTopicResult();
            $review_topic->setTopic($topic);
            $review_topic->setReview($review);
            $this->reviewTopicRepository->save($review_topic, true);
        }

        return;
    }
}
