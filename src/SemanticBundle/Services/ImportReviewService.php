<?php

namespace SemanticBundle\Services;

use SemanticBundle\Entity\Review;
use SemanticBundle\Entity\HotelStats;
use Doctrine\ORM\EntityRepository;
use Ddeboer\DataImport\Reader\CsvReader;
use SplFileObject;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Finder\SplFileInfo;
use SemanticBundle\Interfaces\CsvImportInterface;

/**
 * Class ImportReviewService
 * @package SemanticBundle\Services
 * @implements CsvImportInterface
 */
class ImportReviewService implements CsvImportInterface
{
    /**
     * @var EntityRepository
     */
    private $reviewRepository;

    /**
     * @var EntityRepository
     */
    private $hotelRepository;

    /**
     * ImportReviewService constructor.
     * @param EntityRepository $reviewRepository
     * @param EntityRepository $hotelRepository
     */
    public function __construct(
        EntityRepository $reviewRepository,
        EntityRepository $hotelRepository
    ) {
        $this->reviewRepository = $reviewRepository;
        $this->hotelRepository = $hotelRepository;
    }

    /**
     * @param SplFileInfo $file
     */
    public function importFromCsv($file)
    {
        $csv_file = new SplFileObject($file->getPathName());
        $reader = new CsvReader($csv_file);
        $reader->setHeaderRowNumber(0);
        $reader->setStrict(false);
        $hotel = null;
        foreach ($reader as $row) {
            $criteria = array('id' => intval($row['HotelId']));
            $hotel = $this->hotelRepository->findOneOrCreate($criteria);

            $criteria = array(
                'review' => $row['Review'],
                'hotel' => $hotel['entity'],
            );
            $review = $this->reviewRepository->findOneOrCreate($criteria);
            $hotel = $hotel['entity'];
            $hotel->setTotalReviews($hotel->getTotalReviews() + 1);
            $this->hotelRepository->updateIfNew($hotel, $review['created']);

        }

        return;
    }
}
