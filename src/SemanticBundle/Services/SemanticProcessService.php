<?php


namespace SemanticBundle\Services;

use Doctrine\Common\Inflector\Inflector;
use SemanticBundle\Entity\Review;
use SemanticBundle\Entity\Topic;
use SemanticBundle\Entity\Word;
use SemanticBundle\Utils\TextSanitize;
use SemanticBundle\Interfaces\SemanticProcessInterface;
use Symfony\Component\Security\Acl\Exception\Exception;

class SemanticProcessService implements SemanticProcessInterface
{

    /**
     * @var array
     */
    private $positiveWords;

    /**
     * @var array
     */
    private $negativeWords;


    /**
     * @var array
     */
    private $negativeArray = array();

    /**
     * positive array
     * @var array
     */
    private $positiveArray = array();


    /**
     * topic array
     * @var array
     */
    private $topicArray = array();


    /**
     * @var TextSanitize
     */
    private $textSanitize;

    public function __construct(TextSanitize $textSanitize)
    {
        $this->textSanitize = $textSanitize;

        return;
    }

    /**
     * @param array $positiveWords
     * @param array $negativeWords
     * @param array $stopWords
     * @param array $topics
     * @param array $reviews
     * @return array
     */
    public function extract(
        array $positiveWords,
        array $negativeWords,
        array $stopWords,
        array $topics,
        array $reviews
    ) {
        $result = array();
        $this->positiveWords = $this->prepareWords($positiveWords);
        $this->negativeWords = $this->prepareWords($negativeWords);
        $removeMatch = $this->prepareWords($stopWords);
        $topicsArray = $this->prepareTopic($topics);
        $positiveMatch = $this->preparePattern($this->positiveWords);
        $negativeMatch = $this->preparePattern($this->negativeWords);
        $stopMatch = $this->preparePattern($removeMatch);
        $topicMatch = $this->preparePattern($topicsArray);
        foreach ($reviews as $review) {
            if ($review instanceof Review) {
                $result[] = $this->processReview(
                    $review,
                    $positiveMatch,
                    $negativeMatch,
                    $stopMatch,
                    $topicsArray,
                    $topicMatch
                );
            }
        }

        return $result;
    }

    private function processReview(
        Review $reviewObject,
        $positiveMatch,
        $negativeMatch,
        $stopMatch,
        $topics,
        $topicMatch
    ) {
        $this->positiveArray = array();
        $this->negativeArray = array();
        $this->topicArray = array();

        $review = $reviewObject->getReview();
        $review = trim(mb_strtolower($this->textSanitize->sanitize($review)));


        $review = preg_replace($stopMatch, '', $review);
        $review = $this->prepareReview($review);
        $result = preg_split('/[.,]/', $review);
        foreach ($result as $item) {

            $this->findPositiveMatch($item, $positiveMatch);
            $this->findNegativeMatch($item, $negativeMatch);
            $this->findNegationMatch($item);
            $this->findTopics($item, $topics, $topicMatch);
        }

        $finalResult = array(
            "topics" => $this->topicArray,
            "review_id" => $reviewObject->getId(),
            "attributes" => array("positive" => $this->positiveArray, "negative" => $this->negativeArray),

        );


        return $finalResult;
    }


    /**
     * singularize all sentence
     * @param string $review
     * @return string
     */

    private function prepareReview($review) {
        $words = explode(' ',$review);
        $singularize = array();
        foreach ($words as $word) {
            if ($word !== '') {
                $singularize[] =Inflector::singularize($word);
            }
        }

        return implode(' ',$singularize);


    }

    /**
     * @param array $word
     * @return array
     */
    private function prepareWords(array $word)
    {
        $response = array();
        foreach ($word as $result) {
            if ($result instanceof Word) {
                $response[$result->getName()] = $result->getScore();
            }
        }

        return $response;
    }

    /**
     * @param array $topics
     * @return array
     */
    private function prepareTopic(array $topics)
    {
        $topic = array();
        if (count($topics) > 0) {
            foreach ($topics as $result) {
                if ($result instanceof Topic) {
                    $child = array();
                    foreach ($result->getChildTopic() as $element) {
                        $child[] = $element->getName();
                    }
                    $topic[$result->getName()] = $child;
                }
            }
        }

        return $topic;
    }


    /**
     * @param array $results
     * @return string
     */
    private function preparePattern(array $results)
    {
        $res = array_keys($results);
        $join = implode('|', $res);

        return '/\b('.$join.')\b/';
    }

    /**
     * @param $item
     * @param $topics
     * @param $topicMatch
     */
    private function findTopics(&$item, $topics, $topicMatch)
    {
        foreach ($topics as $key => $value) {
            $item = str_replace($value, $key, $item);
        }
        preg_match_all($topicMatch, $item, $topic_elements, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);
        if (count($topic_elements) > 0) {
            foreach ($topic_elements as $element) {
                $this->topicArray[] = $element[0][0];
            }
        }
    }


    /**
     * @param $item
     * @param $match
     */
    private function findPositiveMatch(&$item, $match)
    {
        preg_match_all($match, $item, $positive_elements, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);
        if (count($positive_elements) > 0) {
            foreach ($positive_elements as $element) {
                $pattern = $element[0][0];
                $new_string = substr($item, 0, $element[0][1]);
                $new_string = str_replace("n't" ," no",$new_string);
                if (strpos($new_string, 'no') !== false) {
                    $this->negativeArray[$new_string.$pattern] = $this->positiveWords[$pattern] * -1;
                } else {
                    $this->positiveArray[$pattern] = $this->positiveWords[$pattern];
                }
            }
        }

        return;
    }

    /**
     * @param $item
     * @param $match
     */
    private function findNegativeMatch(&$item, $match)
    {
        preg_match_all($match, $item, $negative_elements, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);

        if (count($negative_elements) > 0) {
            foreach ($negative_elements as $element) {
                $pattern = $element[0][0];
                $new_string = substr($item, 0, $element[0][1]);
                $new_string = str_replace("n't" ," no ",$new_string);
                if (strpos($new_string, 'no') !== false) {
                    $this->positiveArray[$new_string.$pattern] = $this->negativeWords[$pattern] * -1;
                } else {
                    $this->negativeArray[$pattern] = $this->negativeWords[$pattern];
                }
            }
        }

        return;
    }

    /**
     * @param $item
     */
    private function findNegationMatch(&$item)
    {
        /**
         * Remove positive and negative match from
         * text to explore
         */
        if (is_array($this->positiveArray)) {
            $item = str_replace(array_keys($this->positiveArray), '', $item);
        }
        if (is_array($this->negativeArray)) {
            $item = str_replace(array_keys($this->negativeArray), '', $item);
        }

        $negative = preg_split('/\b(no|not)\b/', $item, null, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        if (count($negative) > 1) {
            $first_element = '';
            if (strpos($negative[0], 'no') === false) {
                $first_element = array_shift($negative);
            }
            $negation_sentence = '';
            foreach ($negative as $element) {
                if (strpos($element, 'no') !== false) {
                    if ($negation_sentence !== '') {
                        $negation_sentence = $this->textSanitize->sanitize($negation_sentence);
                        $this->negativeArray[$negation_sentence] = -1;
                        $negation_sentence = '';
                    }
                    $negation_sentence .= ($first_element == '') ? $element : $first_element.$element;
                    $first_element = '';
                } else {
                    $negation_sentence .= $element;
                }
            }
            $this->negativeArray[$this->textSanitize->sanitize($negation_sentence)] = -1;
        }

        return;
    }
}
