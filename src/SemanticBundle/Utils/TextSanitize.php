<?php


namespace SemanticBundle\Utils;

class TextSanitize
{
    public function sanitize($string)
    {
        return preg_replace('~[^\\pL\d.,]+~u', ' ', mb_strtolower(trim($string)));
    }
}
