<?php


namespace SemanticBundle\Tests\Repository;

use PHPUnit\Framework\TestCase;
use SemanticBundle\Entity\Topic;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TopicRepositoryTest extends KernelTestCase
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()->get('doctrine')->getManager();
    }


    public function testFindOne()
    {

        $wordRepository = $this->em ->getRepository('SemanticBundle:Topic');
        $criteria= array('id'=>'1');
        $wordRepository->findOneOrCreate($criteria);

    }
    /**
     * @expectedException \Exception
     */
    public function testUpdate()
    {

        $wordRepository = $this->em ->getRepository('SemanticBundle:Topic');
        $criteria= array('id'=>'-11');
        $wordRepository->update($criteria);

    }
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}
