<?php

namespace SemanticBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Finder\Finder;

class ReviewControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/review/');
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
    }


    public function testInsert()
    {
        $client = static::createClient();
        $url = '/review/edit';
        $method = 'POST';

        $data = array(
            'oper' => 'add',
            'hotel' => '2',
            'review' => 'Esta review mola un monton',
        );
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);
        $review = $responseData['result']['entity'];
        $this->assertSame('Esta review mola un monton', $review['review']);


        $data = array(
            'oper' => 'add',
            'id' => $review['id'],
            'hotel' => '2',
            'review' => 'Esta review mola un monton',
        );
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);
        $review = $responseData['result']['entity'];
        $this->assertSame('Esta review mola un monton', $review['review']);


        $data = array('oper' => 'add', 'hotel' => '2');
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('KO', $responseData['status']);


    }

    public function testDelete()
    {
        $client = static::createClient();
        $url = '/review/edit';
        $method = 'POST';

        $data = array('oper' => 'add', 'hotel' => '1', 'review' => 'Esta review mola un monton');
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);
        $review = $responseData['result']['entity'];
        $this->assertSame('Esta review mola un monton', $review['review']);
        $data = array('oper' => 'del', 'id' => $review['id']);
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());

    }

    public function testUniqueAlternateSearch()
    {


        $client = static::createClient();
        $method = 'GET';
        $url = '/review/';
        $criteria = array(
            'searchField' => 'id',
            'searchString' => '2',
            'searchOper' => 'eq',
            '_search' => 'true',
        );

        $crawler = $client->request($method, $url, $criteria);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame(1, $responseData['total']);
        $criteria = array(
            'searchField' => 'id',
            'searchString' => '',
            'searchOper' => 'nu',
            '_search' => 'true',
        );

        $crawler = $client->request($method, $url, $criteria);
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertEquals(0, $responseData['total']);


    }


    public function testUpload()
    {
        $client = static::createClient();
        $client->followRedirects();
        $finder = new Finder();
        $finder->files()->in(__DIR__.'/../../Resources/datasets')->name('reviews.csv');
        $this->assertNotSame(0, $finder->count(), 'file not exists');
        $crawler = $client->request('GET', '/');
        foreach ($finder as $filePath) {
            $form = $crawler->selectButton('Upload')->form(array('upload_review[file]' => $filePath));
            $client->submit($form);
            $response = $client->getResponse();
            $this->assertSame(200, $response->getStatusCode());
        }
    }

    public function testUploadFail()
    {
        $client = static::createClient();
        $client->followRedirects();
        $finder = new Finder();
        $finder->files()->in(__DIR__.'/../../Resources/datasets')->name('reviews.csv');
        $this->assertNotSame(0, $finder->count(), 'file not exists');
        $crawler = $client->request('GET', '/');
        foreach ($finder as $filePath) {
            $form = $crawler->selectButton('Upload')->form(array('upload_review[file]' => ''));
            $client->submit($form);
            $response = $client->getResponse();
            $this->assertSame(200, $response->getStatusCode());
        }
    }

    public function testUploadNoForm()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/review/upload');
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
    }

    public function testRun()
    {
        $client = static::createClient();

        $crawler = $client->request('POST', '/review/run');
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);
    }

    public function testFullRun()
    {
        $client = static::createClient();

        $crawler = $client->request('POST', '/review/full_run');
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());

        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);
    }

}
