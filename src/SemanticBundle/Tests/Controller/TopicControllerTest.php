<?php

namespace SemanticBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Finder\Finder;

class TopicControllerTest extends WebTestCase
{

    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/topic/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testList()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/topic/list');
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
    }

    public function testUniqueSearch()
    {
        $client = static::createClient();
        $method = 'GET';
        $url = '/topic/list';
        $criteria = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'eq',
            '_search' => 'true',
        );

        $crawler = $client->request($method, $url, $criteria);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame(1, $responseData['total']);
        $criteria = array(
            'searchField' => 'id',
            'searchString' => '',
            'searchOper' => 'nu',
            '_search' => 'true',
        );

        $crawler = $client->request($method, $url, $criteria);
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertEquals(0, $responseData['total']);
    }

    public function testSearch()
    {
        $client = static::createClient();
        $method = 'GET';
        $url = '/topic/list';


        $criteria = array();
        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'ne',
            '_search' => 'true',
        );

        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'bn',
            '_search' => 'true',
        );

        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'bw',
            '_search' => 'true',
        );
        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'ew',
            '_search' => 'true',
        );

        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'en',
            '_search' => 'true',
        );
        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'cn',
            '_search' => 'true',
        );
        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'nc',
            '_search' => 'true',
        );
        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'nn',
            '_search' => 'true',
        );

        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1,5,10',
            'searchOper' => 'in',
            '_search' => 'true',
        );
        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1,2,3',
            'searchOper' => 'ni',
            '_search' => 'true',
        );
        foreach ($criteria as $data) {

            $crawler = $client->request($method, $url, $data);
            $response = $client->getResponse();
            $this->assertEquals(200, $response->getStatusCode());
            $responseData = json_decode($response->getContent(), true);
            $this->assertGreaterThan(1, $responseData['total'], "oper ".$data['searchOper']." get the error");

        }

    }

    public function testUpload()
    {
        $client = static::createClient();
        $client->followRedirects();
        $finder = new Finder();
        $finder->files()->in(__DIR__.'/../../Resources/datasets')->name('topic.csv');
        $this->assertNotSame(0, $finder->count(), 'file not exists');
        $crawler = $client->request('GET', '/topic/');
        foreach ($finder as $filePath) {
            $form = $crawler->selectButton('Upload')->form(array('upload_topic[file]' => $filePath));
            $client->submit($form);

            $response = $client->getResponse();
            $this->assertSame(200, $response->getStatusCode());
        }
    }

    public function testUploadFail()
    {
        $client = static::createClient();
        $client->followRedirects();
        $finder = new Finder();
        $finder->files()->in(__DIR__.'/../../Resources/datasets')->name('topic.csv');
        $this->assertNotSame(0, $finder->count(), 'file not exists');
        $crawler = $client->request('GET', '/topic/');
        $form = $crawler->selectButton('Upload')->form(array('upload_topic[file]' => ''));
        $client->submit($form);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());

    }

    public function testUploadNoForm()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/topic/upload_topic');
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
    }


    public function testInsert()
    {
        $client = static::createClient();
        $url = '/topic/list/edit';
        $method = 'POST';

        $data = array('oper' => 'add', 'name' => 'room4');
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);
        $topic = $responseData['result']['entity'];

        $this->assertSame('room4', $topic['name']);
        $this->assertSame(true, $responseData['result']['created']);


        $data = array('oper' => 'add', 'name' => 'room4', 'id' => $topic['id']);
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);

        $this->assertSame('OK', $responseData['status']);
        $topic = $responseData['result']['entity'];
        $this->assertSame('room4', $topic['name']);
        $this->assertSame(false, $responseData['result']['created']);


        $data = array('oper' => 'add');
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('KO', $responseData['status']);


    }

    public function testDelete()
    {
        $client = static::createClient();
        $url = '/topic/list/edit';
        $method = 'POST';

        $data = array('oper' => 'add', 'name' => 'room4');
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);
        $topic = $responseData['result']['entity'];
        $this->assertSame('room4', $topic['name']);


        $data = array('oper' => 'del', 'id' => $topic['id']);
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);


    }

    public function testUpdate()
    {
        $client = static::createClient();
        $url = '/topic/list/edit';
        $method = 'POST';

        $data = array('oper' => 'add', 'name' => 'room4');
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);
        $topic = $responseData['result']['entity'];
        $this->assertSame('room4', $topic['name']);


        $data = array('oper' => 'edit', 'id' => $topic['id'], 'name' => 'test1');
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);
        $topic = $responseData['result']['entity'];
        $this->assertSame('test1', $topic['name']);
        $this->assertSame($topic['id'], $topic['id']);


        $data = array('oper' => 'edit', 'id' => $topic['id']);
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('KO', $responseData['status']);

        $data = array('oper' => 'edit', 'name' => 'test1');
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);

    }

    public function testListAlternate()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/topic/list/1');
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertGreaterThan(1, $responseData['total']);
    }

    public function testInsertAlternate()
    {

        $client = static::createClient();
        $url = '/topic/list/1/edit';
        $method = 'POST';

        $data = array('oper' => 'add', 'name' => 'room4');
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);
        $topic = $responseData['result']['entity'];
        $this->assertSame('room4', $topic['name']);


    }

    public function testInsertAlternateFail()
    {

        $client = static::createClient();
        $url = '/topic/list/-1/edit';
        $method = 'POST';

        $data = array('oper' => 'add', 'name' => 'room4');
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('KO', $responseData['status']);;


    }

    public function testDeleteAlternate()
    {

        $client = static::createClient();
        $url = '/topic/list/1/edit';
        $method = 'POST';

        $data = array('oper' => 'add', 'name' => 'room4');
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);
        $topic = $responseData['result']['entity'];
        $this->assertSame('room4', $topic['name']);

        $data = array('oper' => 'del', 'id' => $topic['id']);
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);


    }

    public function testUpdateAlternate()
    {

        $client = static::createClient();
        $url = '/topic/list/1/edit';
        $method = 'POST';

        $data = array('oper' => 'add', 'name' => 'room4');
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);
        $topic = $responseData['result']['entity'];
        $this->assertSame('room4', $topic['name']);

        $data = array('oper' => 'edit', 'id' => $topic['id'], 'name' => 'room41');
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $topic = $responseData['result']['entity'];
        $this->assertSame('room41', $topic['name']);


    }

    public function testUniqueAlternateSearch()
    {


        $client = static::createClient();
        $method = 'GET';
        $url = '/topic/list/1';
        $criteria = array(
            'searchField' => 'id',
            'searchString' => '2',
            'searchOper' => 'eq',
            '_search' => 'true',
        );

        $crawler = $client->request($method, $url, $criteria);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame(1, $responseData['total']);
        $criteria = array(
            'searchField' => 'id',
            'searchString' => '',
            'searchOper' => 'nu',
            '_search' => 'true',
        );

        $crawler = $client->request($method, $url, $criteria);
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertEquals(0, $responseData['total']);


    }


}
