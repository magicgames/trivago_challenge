<?php

namespace SemanticBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Finder\Finder;

class WordControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/word/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testList()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/word/list');
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
    }

    public function testSearchUnique()
    {


        $client = static::createClient();
        $method = 'GET';
        $url = '/word/list';


        $criteria = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'eq',
            '_search' => 'true',
        );

        $crawler = $client->request($method, $url, $criteria);
        $response = $client->getResponse();

        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame(1, $responseData['total']);
        $criteria = array(
            'searchField' => 'id',
            'searchString' => '1,2,3',
            'searchOper' => 'nu',
            '_search' => 'true',
        );

    }

    public function testSearch()
    {
        $client = static::createClient();
        $method = 'GET';
        $url = '/word/list';


        $criteria = array();
        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'ne',
            '_search' => 'true',
        );

        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'bn',
            '_search' => 'true',
        );

        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'bw',
            '_search' => 'true',
        );
        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'ew',
            '_search' => 'true',
        );

        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'en',
            '_search' => 'true',
        );
        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'cn',
            '_search' => 'true',
        );
        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'nc',
            '_search' => 'true',
        );
        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1',
            'searchOper' => 'nn',
            '_search' => 'true',
        );

        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1,2,3',
            'searchOper' => 'in',
            '_search' => 'true',
        );
        $criteria[] = array(
            'searchField' => 'id',
            'searchString' => '1,2,3',
            'searchOper' => 'ni',
            '_search' => 'true',
        );
        foreach ($criteria as $data) {

            $crawler = $client->request($method, $url, $data);
            $response = $client->getResponse();
            $this->assertEquals(200, $response->getStatusCode());
            $responseData = json_decode($response->getContent(), true);
            $this->assertGreaterThan(1, $responseData['total'], "oper ".$data['searchOper']." get the error");

        }


    }

    public function testInsert()
    {
        $client = static::createClient();
        $method = 'POST';
        $url = '/word/edit';
        $data = array('oper' => 'add', 'name' => 'word1ab', 'score' => '1');
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);
        $word = $responseData['result']['entity'];
        $this->assertSame('word1ab', $word['name']);


        $data = array('oper' => 'add', 'name' => 'word1ab', 'score' => '1', 'id' => $word['id']);
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);
        $word = $responseData['result']['entity'];
        $this->assertSame('word1ab', $word['name']);


        $data = array('oper' => 'add', 'name' => 'word1abc', 'score' => 'a');
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('KO', $responseData['status']);

        $data = array('oper' => 'add', 'name' => 'word1abcf');
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('KO', $responseData['status']);

    }

    public function testDelete()
    {
        $client = static::createClient();
        $method = 'POST';
        $url = '/word/edit';
        /**
         * Create
         */
        $data = array('oper' => 'add', 'name' => 'word2', 'score' => 1);
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);
        $word = $responseData['result']['entity'];
        /**
         * Delete
         */
        $data = array('oper' => 'del', 'id' => $word['id']);
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        /**
         * Search
         */

        $url = '/word/list';
        $method = 'GET';
        $data = array(
            'searchField' => 'id',
            'searchString' => $word['id'],
            'searchOper' => 'eq',
            '_search' => 'true',
        );
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame(0, $responseData['total']);
    }

    public function testUpdate()
    {
        $client = static::createClient();
        $method = 'POST';
        $url = '/word/edit';


        $data = array('oper' => 'add', 'name' => 'word2', 'score' => 1);
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame('OK', $responseData['status']);
        $word = $responseData['result']['entity'];


        $data = array('oper' => 'edit', 'id' => $word['id'], 'name' => 'word2a', 'score' => 2);
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());


        $data = array('oper' => 'edit', 'name' => 'word2a', 'score' => 2);
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());


        $data = array('oper' => 'edit', 'id' => '-1', 'name' => 'word2a', 'score' => 2);
        $crawler = $client->request($method, $url, $data);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());


    }

    public function testUpload()
    {
        $client = static::createClient();
        $client->followRedirects();
        $finder = new Finder();
        $finder->files()->in(__DIR__.'/../../Resources/datasets')->name('topic.csv');
        $this->assertNotSame(0, $finder->count(), 'file not exists');
        $crawler = $client->request('GET', '/word/');
        foreach ($finder as $filePath) {
            $form = $crawler->selectButton('Upload')->form(array('upload_word[file]' => $filePath));
            $client->submit($form);
            $response = $client->getResponse();
            $this->assertSame(200, $response->getStatusCode());
        }
    }


    public function testUploadFail()
    {
        $client = static::createClient();
        $client->followRedirects();
        $finder = new Finder();
        $finder->files()->in(__DIR__.'/../../Resources/datasets')->name('topic.csv');
        $this->assertNotSame(0, $finder->count(), 'file not exists');
        $crawler = $client->request('GET', '/word/');
        $form = $crawler->selectButton('Upload')->form(array('upload_word[file]' => ''));
        $client->submit($form);
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());

    }

    public function testUploadNoForm()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/word/upload');
        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode());
    }
}
