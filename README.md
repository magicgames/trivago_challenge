Semantic
========

A Symfony project created on May 11, 2017, 6:46 am.


Semantic word analysis.
=======================

Some changes that I made in the case study:

* Topics and Attributes can be uploaded, using CSV file , in each page you will see an example of the file

* There are 3 types of Attributes: Positives , Negatives and StopWords.

    * Positives: By default if you upload CSV, Score = 1 and if you add manually you can decide the score always > 0 
    * Negatives: By default if you upload the CSV, Score = -1 and if you add manually you can decide the score always < 0
    * StopWords : Score = 0. The reason for the stop words is mitigate False positives: Example "well" is positive "as well as" is not a attribute.
    
    
* In review  section you've 2 action buttons: 
    * Run semantic analysis : This action make a semantic analysis in a new reviews
    * Run semantic full analysis: Mark all reviews as new reviews and make an semantic analysis.
    
    
*  Semantic analysis works different as your primary approach:
    * First step: pass all words to singular.
    * Second step: remove all StopWrods.
    * Third step: make analysis
    
        * If find a Positive Match but there are a "NO / NOT" word before match .. will consider a negative match: Example "Not quite clean" and added in the database with Score = -1 * Match score.
    
        * If find a Negative Match but there are a "NO / NOT" word before match .. will consider a positive match: Example "Not bad" and added in the database with Score = -1 * Match score.
        
        * Replace n't to  not, with this change will be able to find something like "don't feel good" where feel good is positive but don't feel good is negative.
        
        * After make all test will check for words "NO / NOT" matches and will consider a negative match: Example "No elevators", and added in the database with Score = -1.
    
Those values can be changed after in Attributes section. You will see a column called "Auto populated".
* if attribute comes from manual entries or upload by file will be false
* if attribute comes from review analysis will be true

