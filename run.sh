#!/bin/bash -e


echo "#######################################################"
echo "  RUN SCRIPT                                           "
echo "#######################################################"

composer install
php app/console cache:clear
php app/console assets:install
php app/console assetic:dump
php app/console doctrine:database:create
php app/console doctrine:migrations:migrate

echo "#######################################################"
echo "  CREATE TEST DATABASE                                 "
echo "#######################################################"

php app/console doctrine:schema:update --force --env test
php app/console doctrine:fixtures:load -n --env test

echo "#######################################################"
echo "  TESTS                                               "
echo "#######################################################"
phpunit -c app/ --debug --coverage-html coverage/
#phpunit -c app/ --debug
php app/console server:start --force
